
import React from 'react';
import { StyleSheet, View, Text }from 'react-native'; //importar componentes de React Native

export default class CalcDisplay extends React.Component {

  static defaultProps = {
    display: "0", //salida del display por defecto
  }

  render() { //renderizamos el display
    return (
      <View style={styles.container}>
        <Text style={styles.display}>{this.props.display}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:  { padding: 20, },
  display:    { fontSize: 100, color: "white", textAlign: "right", }, //tamaño del resultado, color y posición
})